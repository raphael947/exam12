import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService} from '../auth.service';
import { booksService } from '../books.service';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  bookTextFrombook = "Text not so far";
  showText($event){
    this.bookTextFrombook = $event;
  }

  addbook(){
    this.booksService.addbook(this.text);
    this.text = '';
  }
  /*books= [
    {"text": "Study json", "id":1},
    {"text": "Do final project", "id":2}];
    */
   books= [];
   text:string;
  constructor(private db:AngularFireDatabase, public authService:AuthService, private booksService:booksService) { }

  ngOnInit() {

    this.authService.user.subscribe(user=>{

      this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe(
        books =>{
          this.books = [];
          books.forEach(
            book =>{
              let y = book.payload.toJSON();
              y["$key"] = book.key;
              this.books.push(y);
            }
            
          )
        }
      )

    })
  
    
  }

}