
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { booksService } from '../books.service';

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked = new EventEmitter <any>();
  key;
  id;
  text;
  tempText;


  

  showTheButton = false; // אחראי על הצגת כפתורים
  showEditField = false;

  showEdit(){
       this.showEditField = true;
        this.tempText = this.text;
      }
    
      save(){
        this.booksService.update(this.key,this.text)
        this.showEditField = false;
      }
    
      cancel(){
        this.showEditField = false;
        this.text = this.tempText;
      }
  showButton(){
    this.showTheButton = true ;
  }
  hideButton(){
    this.showTheButton = false ;
  }
  send(){
    console.log("function work");
    this.myButtonClicked.emit(this.text);
  }
  delete(){
    this.booksService.delete(this.key)
  }
  constructor(private booksService:booksService) { }
  
 //id;
  ngOnInit() {
    this.text = this.data.text;
   // this.id = this.data.id;
   this.key = this.data.$key;
  }

}
