import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";
///////////////////////////////////////////




@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})


export class RegistrationComponent implements OnInit {

 
  

  email: string;
  password: string;
  password2: string;

  nickname: string;
  name:string;
  error='';
  output='';
  code='';
  message='';
  
 
  required = '';//הדפסת הערה שדה חובה
  require = true; //דגל עבור שדה חובה- אם הכל מלא טרו
  flag = false; //דגל עבור שגיאה (6 תווים בסיסמה וכו)- אם אין פולס
  specialChar = ['!','@','#','$','%','^','&','*','(',')','_','-','+','=']; //תווים נדרשים-דרושים תווים מיוחדים
  valid = false; //דגל עמידה בסיסמה- אם לא עומדים בכל התנאים פולס
  validation = ""; //הודעה לתווים מיוחדים

  

  signup(){
    this.authService.signup(this.email,this.password)
    .then(value =>{
      this.authService.updateProfile(value.user,this.name);
      this.authService.addUser(value.user,this.name);
    }).then(value=>{
      this.router.navigate(['/']);

    }).catch(err => {
      this.error = err.code;
      console.log(err);
      this.output = err.message;

    })
  }

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  
  }



}
