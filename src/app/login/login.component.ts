import { Component, OnInit, Input } from '@angular/core';
import {AuthService} from '../auth.service';
import { Router } from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
  error='';
  email:string;
  password:string;
  output = '';
  code='';
  message='';

  login(){
    this.authService.login(this.email,this.password)
      .then(user=>{
        this.router.navigate(['/books']);
      }).catch(err=>{
        this.error = err.code;
        this.output = err.message;
      })
    }

  constructor(private authService: AuthService, private router:Router) { }

  ngOnInit() {
  }

}
